// Function to write text before game starts
let i = 0;
let txt = 'Rock, Paper, Scissors?';
let speed = 50;

function typeWriter() {
    if (i < txt.length) {
        document.getElementById("gameH1").innerHTML += txt.charAt(i);
        i++;
        setTimeout(typeWriter, speed);
    }
}

// Function to play main button audio on click
function playSound() {
    const buttonPress = document.querySelector("#startbuttonsound");
    buttonPress.play();
}
mainButton = document.querySelector("#startbutton").addEventListener("click", playSound);

// Function to play winner audio on win
function playWinSound() {
    const winSound = document.getElementById("winnersound");
    winSound.play();
}

// Function to play loser audio on loss
function playLoseSound() {
    const loseSound = document.getElementById("losersound");
    loseSound.play();
}

// Function to hide start div and show end div
let startContainer = document.getElementById('startContainer');
let btn = document.querySelector("#startButton");

btn.addEventListener('click', function(){
    startContainer.style.opacity = 0;
    startContainer.style.transform = 'scale(0)';
    // Add timeout with length matching animation-duration 
    window.setTimeout(function(){
        startContainer.style.display = 'none';
    },700); 
    setTimeout(() => {  typeWriter(); }, 1000);
    // Add event listener to all of the game buttons
    gameButtons = document.querySelectorAll(".gameSelection").forEach(item => {
        item.addEventListener("click", playSound);
    })
});


// Function to hide end div and show winner div & logos
function hideEndContainerShowWinner() {
    let gameContainer = document.querySelector('.gameContainer');
    let bottomContainer = document.getElementById('bottomContainer');
    gameContainer.style.opacity = 0;
    gameContainer.style.transform = 'scale(0)';
    // Add timeout with length matching animation-duration 
    gameContainer.style.display = 'none';
    bottomContainer.style.display = 'block';
}

btn.addEventListener('click', function(){
    startContainer.style.opacity = 0;
    startContainer.style.transform = 'scale(0)';
    // Add timeout with length matching animation-duration 
    window.setTimeout(function(){
        startContainer.style.display = 'none';
    },700); 
    setTimeout(() => {  typeWriter(); }, 1000);
    // Add event listener to all of the game buttons
    gameButtons = document.querySelectorAll(".gameSelection").forEach(item => {
        item.addEventListener("click", playSound);
    })
});

let playerScore = 0;
let computerScore = 0;
let gamesPlayed = 0;

document.addEventListener("click", gameSelectionListener);
// See if the click was on a game selection button (can't apply directly on button as div initially hidden)
function gameSelectionListener(event) {
    let element = event.target;
    console.log(event.target);
    let rock = "rock";
    let paper = "paper";
    let scissors = "scissors";
    if (element.classList.contains("gameSelection") && element.id === ("rockDiv")) {
        playRound(rock);
        console.log("submitted rock")
    }
    else if (element.classList.contains("selection") && element.id === ("rockImg")) {
        playRound(rock);
        console.log("submitted rock")
    }
    else if (element.classList.contains("gameSelectionText") && element.id === ("rockSelection")) {
        playRound(rock);
        console.log("submitted rock")
    }
    else if (element.classList.contains("gameSelection") && element.id === ("paperDiv")) {
        playRound(paper);
        console.log("submitted paper")
    }
    else if (element.classList.contains("selection") && element.id === ("paperImg")) {
        playRound(paper);
        console.log("submitted paper")
    }
    else if (element.classList.contains("gameSelectionText") && element.id === ("paperSelection")) {
        playRound(paper);
        console.log("submitted paper")
    }
    else if (element.classList.contains("gameSelection") && element.id === ("scissorsDiv")) {
        playRound(scissors);
        console.log("submitted scissors")
    }
    else if (element.classList.contains("selection") && element.id === ("scissorsImg")) {
        playRound(scissors);
        console.log("submitted scissors")
    }
    else if (element.classList.contains("gameSelectionText") && element.id === ("scissorsSelection")) {
        playRound(scissors);
        console.log("submitted scissors")
    }
}


// Function to randomly generate computer game entries
function computerPlay() {
    randomGameValue = Math.floor(Math.random() * 3);
    if (randomGameValue == "0") {
        return "rock";
    }
    else if (randomGameValue == "1") {
        return "paper";
    }
    else {
        return "scissors";
    }
}

// Main game function
function playRound(playerSelection) {
    console.log(playerSelection);
    // Generate computer selection
    const computerSelection = computerPlay();
    const rockSelected = document.querySelector("#rockDiv");
    const paperSelected = document.querySelector("#paperDiv");
    const scissorsSelected = document.querySelector("#scissorsDiv");
    const computerRockSelected = document.querySelector("#computerRockDiv");
    const computerPaperSelected = document.querySelector("#computerPaperDiv");
    const computerScissorsSelected = document.querySelector("#computerScissorsDiv");

    // Loop through computer options
    if (computerSelection == "rock") {
        computerRockSelected.style.backgroundColor = "#7987e9";
        computerPaperSelected.style.backgroundColor = "white";
        computerScissorsSelected.style.backgroundColor = "white";
    }
    else if (computerSelection == "paper") {
        computerPaperSelected.style.backgroundColor = "#7987e9";
        computerRockSelected.style.backgroundColor = "white";
        computerScissorsSelected.style.backgroundColor = "white";
    }
    else if (computerSelection == "scissors") {
        computerScissorsSelected.style.backgroundColor = "#7987e9";
        computerRockSelected.style.backgroundColor = "white";
        computerPaperSelected.style.backgroundColor = "white";
    }
    // Convert player entry to string
    let playerSelectionString = String(playerSelection);
    // Convert player selection to all lowercase
    let playerSelectionLowercase = playerSelectionString.toLowerCase();
    // If player selection is rock
    if (playerSelectionLowercase === "rock") {
        rockSelected.style.backgroundColor = "#fc5868";
        paperSelected.style.backgroundColor = "white";
        scissorsSelected.style.backgroundColor = "white";
        // If computer selection is rock then draw
        if (computerSelection === "rock") {
            console.log("Draw!");
            playerScore++;
            computerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "draw";
        }
        // If computer selection is paper then lose as paper beats rock
        else if (computerSelection === "paper") {
            console.log("You lose - paper beats rock!");
            computerScore++;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "lose";
        }
        // If computer selection is scissors then win as scissors beats paper
        else {
            console.log ("You win - scissors beats paper!");
            playerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            game(playerScore, computerScore);
            return "win";
        }
    }
    // Else if player selection is paper
    else if (playerSelectionLowercase === "paper") {
        paperSelected.style.backgroundColor = "#fc5868";
        rockSelected.style.backgroundColor = "white";
        scissorsSelected.style.backgroundColor = "white";
        // If computer selection is rock then win as paper beats rock
        if (computerSelection === "rock") {
            console.log("You win - paper beats rock!");
            playerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            game(playerScore, computerScore);
            return "win";
        }
        // If computer selection is paper then draw
        else if (computerSelection === "paper") {
            console.log("Draw!");
            playerScore++;
            computerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "draw";
        }
        // If computer selection is scissors then lose as scissors beats paper
        else {
            console.log("You lose - scissors beats paper!")
            computerScore++;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "lose";
        }
    }
    // Else (player selected scissors)
    else if (playerSelectionLowercase === "scissors") {
        scissorsSelected.style.backgroundColor = "#fc5868";
        rockSelected.style.backgroundColor = "white";
        paperSelected.style.backgroundColor = "white";
        // If computer selection is rock then lose as rock beats scissors
        if (computerSelection === "rock") {
            console.log("You lose - rock beats scissors!")
            computerScore++;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "lose";
        }
        // If computer selection is paper then win as scissors beats paper
        else if (computerSelection === "paper") {
            console.log("You win - scissors beats paper!");
            playerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            game(playerScore, computerScore);
            return "win";
        }
        // If computer selection is scissors then draw
        else {
            console.log("Draw!");
            playerScore++;
            computerScore++;
            // Set the score in the player HTML element
            const currentPlayerScore = document.querySelector("#playerScore").innerHTML = `Score: ${playerScore}`;
            // Set the score in the computer HTML element
            const currentComputerScore = document.querySelector("#computerScore").innerHTML = `Score: ${computerScore}`;
            game(playerScore, computerScore);
            return "draw";
        }
    }
    else {
        console.log("Invalid entry, please try again.");
        game(playerScore, computerScore);
        return null;
    }
}


// Score tracker
function game(playerScore, computerScore) {
    // Set variable for the game update paragraph tag
    const gameUpdates = document.querySelector(".gameUpdates");
    // Set variables for the user and robot win logos
    let userWinLogo = document.getElementById('userWinsLogo');
    let robotWinLogo = document.getElementById('robotWinsLogo');
    if (playerScore == 5 || computerScore == 5) {
        if (playerScore > computerScore) {
            let text = document.createTextNode(`YOU WIN ${playerScore}:${computerScore}!`);
            gameUpdates.appendChild(text);
            hideEndContainerShowWinner();
            playWinSound();
            userWinLogo.style.display = 'block';
        }
        else if (computerScore > playerScore) {
            let text = document.createTextNode(`COMPUTER WINS ${computerScore}:${playerScore}!`);
            gameUpdates.appendChild(text);
            hideEndContainerShowWinner();
            playLoseSound();
            robotWinLogo.style.display = 'block';
        }
        else {
            let text = document.createTextNode(`IT WAS A DRAW!`);
            gameUpdates.appendChild(text);
            hideEndContainerShowWinner();
            userWinLogo.style.display = 'block';
            robotWinLogo.style.display = 'block';
        }   
    }
}